![Logo UNET](unetLogo.png "Logo de la UNET")

Sopa de letras
===============

### Proyecto III de Computación I (C)

Sopa de letras en CLI, escrito en lenguaje C.
Proyecto desarrollado en Borland C++ Builder.

Para más información, revisar el enunciado en PDF incluido en el repositorio.

#### Desarrollador
* [Gregory Sánchez (@gregsanz182)](https://github.com/gregsanz182)

#### Asignatura
* Nombre: Computación I
* Código: 0415102T
* Profesora: María Valero

*Proyecto desarrollado con propositos educativos para la **Universidad Nacional
Experimental del Táchira***