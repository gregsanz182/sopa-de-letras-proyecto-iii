//---------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <windows.h>
#include <string.h>
#include <vcl.h>
#pragma hdrstop
//---------------------------------------------------------------------------
void words20(char a[][9]);
void x(char a[][20]);
void show(char a[][20],char b[][9],int w[]);
void order(char a[20][20],char b[][9],int w[]);
void move(char a[][20],char b[][9],int w[]);
void credits();
#pragma argsused
int main(int argc, char* argv[])
{
        char wd[20][9],soup[20][20],r;
        int caso,w[5];
        words20(wd);
        randomize();
        while(1){
        textbackground(15);
        clrscr();
        _setcursortype(_NORMALCURSOR);
        textcolor(BLUE);
        gotoxy(28,1);
        cprintf("Sopa de letras - Musica");
        gotoxy(35,2);
        cprintf("%c %c %c %c",14,14,14,14,14,14);
        gotoxy(32,3);
        textcolor(12);
        cprintf(">>>>MENU<<<<");
        gotoxy(9,5);
        textcolor(0);
        cprintf("1- Ver listado de palabras");
        gotoxy(9,6);
        cprintf("2- Nueva sopa de letras");
        gotoxy(9,7);
        cprintf("3- Continuar con la ultima sopa de letras");
        gotoxy(9,8);
        cprintf("4- Creditos");
        gotoxy(9,9);
        cprintf("0- Salir");
        gotoxy(9,11);
        cprintf("Introduzca una opcion valida: ");
        scanf("%d",&caso);
        if(caso==1){
           system("cls");
           for(int i=0;i<20;i++){
            printf("\n\t%s",wd[i]);
           }
           getch();
         }
        if(caso==2){
            x(soup);
            for(int i=0;i<5;i++){
            do{
             r=0;
             w[i]=random(20);
             for(int j=0;j<5;j++){
              if(w[i]==w[j]){
               r++;
              }
             }
            }while(r>1);
            }
            order(soup,wd,w);
            show(soup,wd,w);
            move(soup,wd,w);
        }
        if(caso==3){
         printf("\n\n\t\tNo existe una partida anterior\n\t\tPresione ENTER para continuar");
         getch();
        }
        if(caso==4){
         credits();
         getch();
        }
        if(caso==0){
         break;
        }
        }

        return 0;
}
//---------------------------------------------------------------------------
void words20(char a[][9]){
 strcpy(a[0],"musica");
 strcpy(a[1],"sonidos");
 strcpy(a[2],"nota");
 strcpy(a[3],"bateria");
 strcpy(a[4],"samba");
 strcpy(a[5],"bajo");
 strcpy(a[6],"cuatro");
 strcpy(a[7],"arpa");
 strcpy(a[8],"maracas");
 strcpy(a[9],"acordes");
 strcpy(a[10],"tambor");
 strcpy(a[11],"violin");
 strcpy(a[12],"jazz");
 strcpy(a[13],"rock");
 strcpy(a[14],"clave");
 strcpy(a[15],"cancion");
 strcpy(a[16],"piano");
 strcpy(a[17],"flauta");
 strcpy(a[18],"tono");
 strcpy(a[19],"ritmo");
}
void x(char a[][20]){
        for(int i=0;i<20;i++){
         for(int j=0;j<20;j++){
          a[i][j]='x';
         }
        }
}
void order(char a[20][20],char b[][9],int w[]){
  int pos1,pos2,dir,r,h;
   for(int i=0;i<5;i++){
    h=strlen(b[w[i]]);
    dir=random(8);
    do{
     pos1=random(20);
     pos2=random(20);
     if(dir==2 || dir==5 && (pos1+h)>19){
      pos1=13;
     }
     if(dir==7 && (pos1+h)>19){
      pos1=13;
     }
     if(dir==3 || dir==4 && (pos1-h)<0){
      pos1=6;
     }
     if(dir==6 && (pos1-h)<0){
      pos1=6;
     }
     if(dir==0 || dir==2 && (pos2+h)>19){
      pos2=13;
     }
     if(dir==4 && (pos2+h)>19){
      pos2=13;
     }
     if(dir==1 || dir==3 && (pos2-h)<0){
      pos2=6;
     }
     if(dir==5 && (pos2-h)<0){
      pos2=6;
     }
     r=0;
     if(dir==0){
      for(int x=0;x<h;x++){
       if(a[pos1][pos2+x]=='x' || a[pos1][pos2+x]==b[w[i]][x]){
        r++;
       }
      }
     }
     if(dir==1){
      for(int x=0;x<h;x++){
       if(a[pos1][pos2-x]=='x' || a[pos1][pos2-x]==b[w[i]][x]){
        r++;
       }
      }
     }
     if(dir==2){
      for(int x=0;x<h;x++){
       if(a[pos1+x][pos2+x]=='x' || a[pos1+x][pos2+x]==b[w[i]][x]){
        r++;
       }
      }
     }
     if(dir==3){
      for(int x=0;x<h;x++){
       if(a[pos1-x][pos2-x]=='x' || a[pos1-x][pos2-x]==b[w[i]][x]){
        r++;
       }
      }
     }
     if(dir==4){
      for(int x=0;x<h;x++){
       if(a[pos1-x][pos2+x]=='x' || a[pos1-x][pos2+x]==b[w[i]][x]){
        r++;
       }
      }
     }
     if(dir==5){
      for(int x=0;x<h;x++){
       if(a[pos1+x][pos2-x]=='x' || a[pos1+x][pos2-x]==b[w[i]][x]){
        r++;
       }
      }
     }
     if(dir==6){
      for(int x=0;x<h;x++){
       if(a[pos1-x][pos2]=='x' || a[pos1-x][pos2]==b[w[i]][x]){
        r++;
       }
      }
     }
     if(dir==7){
      for(int x=0;x<h;x++){
       if(a[pos1+x][pos2]=='x' || a[pos1+x][pos2]==b[w[i]][x]){
        r++;
       }
      }
     }
    }while(r!=h);
    if(dir==0){
     for(int x=0;x<h;x++){
      a[pos1][pos2+x]=b[w[i]][x];
     }
    }
    if(dir==1){
     for(int x=0;x<h;x++){
      a[pos1][pos2-x]=b[w[i]][x];
     }
    }
    if(dir==2){
     for(int x=0;x<h;x++){
      a[pos1+x][pos2+x]=b[w[i]][x];
     }
    }
    if(dir==3){
     for(int x=0;x<h;x++){
      a[pos1-x][pos2-x]=b[w[i]][x];
     }
    }
    if(dir==4){
     for(int x=0;x<h;x++){
      a[pos1-x][pos2+x]=b[w[i]][x];
     }
    }
    if(dir==5){
     for(int x=0;x<h;x++){
      a[pos1+x][pos2-x]=b[w[i]][x];
     }
    }
    if(dir==6){
     for(int x=0;x<h;x++){
      a[pos1-x][pos2]=b[w[i]][x];
     }
    }
    if(dir==7){
     for(int x=0;x<h;x++){
      a[pos1+x][pos2]=b[w[i]][x];
     }
    }
   }
   for(int i=0;i<20;i++){
    for(int j=0;j<20;j++){
     if(a[i][j]=='x'){
      a[i][j]=random(26)+97;
     }
    }
   }
}

void show(char a[][20],char b[][9],int w[]){
        textbackground(15);
        clrscr();
        textcolor(BLUE);
        gotoxy(31,2);
        cprintf("Nueva Sopa de letras");
        textcolor(12);
        gotoxy(47,4);
        cprintf("Palabras a encontrar:");
        textcolor(BLACK);
        for(int i=0;i<5;i++){
         gotoxy(69,4+i);
         cprintf("%s",b[w[i]]);
        }
        for(int i=0;i<20;i++){
         for(int j=0;j<20;j++){
          gotoxy((j*2)+5,i+4);
          cprintf("%c",a[i][j]);
         }
        }
        textcolor(CYAN);
        gotoxy(53,12);
        cprintf("Presione X para volver");
        gotoxy(56,13);
        cprintf("al menu principal");
}
void move(char a[][20],char b[][9],int w[]){
 int cont1=0,cont2=0,cont1e,cont1f,cont2e,cont2f,let,ri=0,rf=0,n,r=0,sep=0,win=0;
 _setcursortype(_SOLIDCURSOR);
 do{
 gotoxy(5+cont2,4+cont1);
 fflush(stdin);
 let=getch();
 if(let==72 && cont1>0){
  cont1--;
 }
 if(let==80 && cont1<19){
  cont1++;
 }
 if(let==77 && cont2<38){
  cont2+=2;
 }
 if(let==75 && cont2>0){
  cont2-=2;
 }
 if(let=='x'){
  break;
 }
 if(let=='i'){
  cont1e=cont1;
  cont2e=cont2;
  cont2e=cont2e/2;
  ri=1;
  textcolor(11);
  gotoxy(5+(cont2e*2),4+cont1e);
  cprintf("%c",a[cont1e][cont2e]);
 }
 if(let=='f'){
  cont1f=cont1;
  cont2f=cont2;
  cont2f=cont2f/2;
  rf=1;
 }
 if(ri==1 && rf==1){
  if(cont1e==cont1f){
   //right
   if(cont2e<cont2f){
    n=cont2f-cont2e;
    for(int j=0;j<5;j++){
     for(int i=0;i<(n+1);i++){
      if(a[cont1e][cont2e+i]==b[w[j]][i]){
       r++;
      }
     }
     if(r==(n+1)){
      for(int i=0;i<(n+1);i++){
       gotoxy(5+(i*2)+(cont2e*2),4+cont1e);
       textcolor(RED);
       cprintf("%c",a[cont1e][cont2e+i]);
      }
      textcolor(GREEN);
      gotoxy(69,4+j);
      cprintf("%s",b[w[j]]);
      win++;
      sep=1;
     }
     r=0;
    }
   }
   //oeste
   if(cont2e>cont2f){
    n=cont2e-cont2f;
    for(int j=0;j<5;j++){
     for(int i=0;i<(n+1);i++){
      if(a[cont1e][cont2e-i]==b[w[j]][i]){
       r++;
      }
     }
     if(r==(n+1)){
      for(int i=0;i<(n+1);i++){
       gotoxy(5+(cont2e*2)-(i*2),4+cont1e);
       textcolor(RED);
       cprintf("%c",a[cont1e][cont2e-i]);
      }
      textcolor(GREEN);
      gotoxy(69,4+j);
      cprintf("%s",b[w[j]]);
      win++;
      sep=1;
     }
     r=0;
    }
   }
  }
  if(cont2e==cont2f){
   //sur
   if(cont1e<cont1f){
    n=cont1f-cont1e;
    for(int j=0;j<5;j++){
     for(int i=0;i<(n+1);i++){
      if(a[cont1e+i][cont2e]==b[w[j]][i]){
       r++;
      }
     }
     if(r==(n+1)){
      for(int i=0;i<(n+1);i++){
       gotoxy(5+(cont2e*2),4+cont1e+i);
       textcolor(RED);
       cprintf("%c",a[cont1e+i][cont2e]);
      }
      textcolor(GREEN);
      gotoxy(69,4+j);
      cprintf("%s",b[w[j]]);
      win++;
      sep=1;
     }
     r=0;
    }
   }
   //norte
   if(cont1e>cont1f){
    n=cont1e-cont1f;
    for(int j=0;j<5;j++){
     for(int i=0;i<(n+1);i++){
      if(a[cont1e-i][cont2e]==b[w[j]][i]){
       r++;
      }
     }
     if(r==(n+1)){
      for(int i=0;i<(n+1);i++){
       gotoxy(5+(cont2e*2),4+cont1e-i);
       textcolor(RED);
       cprintf("%c",a[cont1e-i][cont2e]);
      }
      textcolor(GREEN);
      gotoxy(69,4+j);
      cprintf("%s",b[w[j]]);
      win++;
      sep=1;
     }
     r=0;
    }
   }
  }
  if((cont1e-cont2e)==(cont1f-cont2f)){
   //sureste
   if(cont1e<cont1f){
    n=cont1f-cont1e;
    for(int j=0;j<5;j++){
     for(int i=0;i<(n+1);i++){
      if(a[cont1e+i][cont2e+i]==b[w[j]][i]){
       r++;
      }
     }
     if(r==(n+1)){
      for(int i=0;i<(n+1);i++){
       gotoxy(5+(cont2e*2)+(i*2),4+cont1e+i);
       textcolor(RED);
       cprintf("%c",a[cont1e+i][cont2e+i]);
      }
      textcolor(GREEN);
      gotoxy(69,4+j);
      cprintf("%s",b[w[j]]);
      win++;
      sep=1;
     }
     r=0;
    }
   }
   //noroeste
   if(cont1e>cont1f){
    n=cont1e-cont1f;
    for(int j=0;j<5;j++){
     for(int i=0;i<(n+1);i++){
      if(a[cont1e-i][cont2e-i]==b[w[j]][i]){
       r++;
      }
     }
     if(r==(n+1)){
      for(int i=0;i<(n+1);i++){
       gotoxy(5+(cont2e*2)-(i*2),4+cont1e-i);
       textcolor(RED);
       cprintf("%c",a[cont1e-i][cont2e-i]);
      }
      textcolor(GREEN);
      gotoxy(69,4+j);
      cprintf("%s",b[w[j]]);
      win++;
      sep=1;
     }
     r=0;
    }
   }
  }
  if((cont1e+cont2e)==(cont1f+cont2f)){
   //noreste
   if(cont1e<cont1f){
    n=cont1f-cont1e;
    for(int j=0;j<5;j++){
     for(int i=0;i<(n+1);i++){
      if(a[cont1e+i][cont2e-i]==b[w[j]][i]){
       r++;
      }
     }
     if(r==(n+1)){
      for(int i=0;i<(n+1);i++){
       gotoxy(5+(cont2e*2)-(i*2),4+cont1e+i);
       textcolor(RED);
       cprintf("%c",a[cont1e+i][cont2e-i]);
      }
      textcolor(GREEN);
      gotoxy(69,4+j);
      cprintf("%s",b[w[j]]);
      win++;
      sep=1;
     }
     r=0;
    }
   }
   //suroeste
   if(cont1e>cont1f){
    n=cont1e-cont1f;
    for(int j=0;j<5;j++){
     for(int i=0;i<(n+1);i++){
      if(a[cont1e-i][cont2e+i]==b[w[j]][i]){
       r++;
      }
     }
     if(r==(n+1)){
      for(int i=0;i<(n+1);i++){
       gotoxy(5+(cont2e*2)+(i*2),4+cont1e-i);
       textcolor(RED);
       cprintf("%c",a[cont1e-i][cont2e+i]);
      }
      textcolor(GREEN);
      gotoxy(69,4+j);
      cprintf("%s",b[w[j]]);
      win++;
      sep=1;
     }
     r=0;
    }
   }
  }
  ri=0;
  rf=0;
  if(sep!=1){
   textcolor(RED);
   gotoxy(50,16);
   cprintf("La seleccion es incorrecta");
   gotoxy(49,17);
   cprintf("Presione ENTER para continuar");
   getch();
   textcolor(LIGHTGRAY);
   gotoxy(50,16);
   cprintf("La seleccion es incorrecta");
   gotoxy(49,17);
   cprintf("Presione ENTER para continuar");
   gotoxy(5+(cont2e*2),4+cont1e);
   textcolor(BLACK);
   cprintf("%c",a[cont1e][cont2e]);
  }
  sep=0;
 }
 if(win==5){
   textcolor(GREEN);
   gotoxy(57,16);
   cprintf("Felicitaciones");
   gotoxy(58,17);
   cprintf("HAS GANADO%c%c",33,33);
   gotoxy(49,18);
   cprintf("Presione ENTER para continuar");
   getch();
   break;
 }
 }while(1);
}
void credits(){
 system("cls");
 gotoxy(24,7);
 printf("Creado por: Gregory S%cnchez",160);
 gotoxy(24,8);
 printf("C.I.: 20.999.771");
 gotoxy(24,9);
 printf("Ing. Informatica");
 gotoxy(24,10);
 printf("Seccion: 10");
 gotoxy(18,13);
 printf("Presione ENTER para volver al menu principal");
}
